let express = require('express');
let app = express();
let http = require('http').Server(app);
let io = require('socket.io')(http);
let ss = require('socket.io-stream');
let concat = require('concat-stream');
let exec = require('child_process').exec;
let execSync = require('child_process').execSync;
const config = require(__dirname + '/config.js');


let Pis = {};
let hPis = {}; // this needs an overhaul
let PiSocks = {};

var pi = io.of('/pi');
var client = io.of('/client');
var restart = false;

app.use(express.static(__dirname + '/client'));

app.get('/', function(req, res) {
  res.sendfile(__dirname + '/index.html');
});

http.listen(8080, '0.0.0.0', function() {
  console.log(Date() + ' listening on *:8080');
});

pi.on('connection', function(socket) {
  socket.on('meta', tmp => {
    tmp.name = tmp.config.name;
    if (socket.handshake.query.unconfigured)
      tmp.name = `Unconfigured: ${socket.id}`;
    let name = tmp.name;

    Pis[name] = tmp;
    hPis[name.toLowerCase()] = tmp;
    delete tmp
    Pis[name].status = 0;
    PiSocks[name] = socket;
    console.log(Date() + ' RasPi "' + name + '" (' + socket.request.connection.remoteAddress + ') connected.');

    client.emit('change', {
      type: 'piOn',
      data: Pis[name],
      name: name
    });

    socket.on('change', what => {
      if (what.change.config && what.change.config.name) {
          client.emit('change', {
              type: 'deletePi',
              name: name
          });
      } else {
        //Apply Changes to Mirror, sync version, browser support
        (function changeIt(change, level) {
          for (let key in change) {
            if (typeof change[key] === 'object' && level[key]) {
              changeIt(change[key], level[key]);
            } else
              level[key] = change[key];
          }
        })(what.change, Pis[name]);

        //Identify pi
        what.name = name;

        //Forward Changes to Clients
        client.emit('change', what);
      }
    });

    socket.on('data', (data, to) => {
      //Identify pi
      data.name = name;

      //Forward Changes to Clients
      if (client.sockets[to])
        client.sockets[to].emit('data', data);
    });

      socket.on('disconnect', () => {
      //clean up
      if (socket.handshake.query.unconfigured || socket.abandon) {
        client.emit('change', {
          type: 'deletePi',
          name: name
        });
      } else {
        console.log(Date() + ' RasPi "' + name + '" disconnected.');

        //Forward to Client
        client.emit('change', {
          type: 'piOff',
          name: name,
          change: {
            running: -1,
            error: -1,
            status: 1
          }
        });
      }
      
      delete Pis[name];
      delete PiSocks[name];
    });
  });
});

client.on('connection', function(socket) {
  //console.log(Date() + ' Client ' + socket.request.connection.remoteAddress + ' has connected.');
  socket.emit('status', Pis);
  socket.on('command', function(pi, command) {
    let tmp = Pis[pi];
    if (tmp && tmp.status == 0) {
      command.sender = socket.id;
      PiSocks[pi].emit('command', command);
    }
  });
});
