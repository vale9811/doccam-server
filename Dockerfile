FROM node:alpine
WORKDIR /usr/src/app
RUN addgroup -S doccam && adduser -S -g doccam  doccam
COPY package.json .
RUN npm install
USER doccam
COPY . .
EXPOSE 8080
CMD [ "npm", "start" ]