var socket, Pis, tiles;

var tiles = {
    pi: null,
    statusBar: null
}

var helpers = {
    getPanelUrl: function(port) {
        var location = window.location;
        return location.protocol + '//cam' + port + '.' + location.host;
    }
};

var statBar = {
    status: {
        text: ['Online', 'Offline'],
        color: ['success', 'danger'],
        id: 'stat'
    },
    running: {
        text: ['Streaming', 'Paused', 'Locked: Please fix error!'],
        color: ['success', 'warning', 'warning'],
        id: 'stream',
        specialFunction: function(current) {
            if (current.running === 2) {
                $('.pp-' + current.id).hide();
                $('.snapb-' + current.id).hide();
            } else {
                $('.pp-' + current.id).show();
                $('.snapb-' + current.id).show();
            }
        }
    },
    error: {
        text: ['Camera Disconnected', 'Stream-Server Disconnected', 'Wrong ffmpeg executable.', 'Unknown error.', 'Invalid Stream Key or the stream server doesn\'t accept connections.'],
        color: ['danger', 'danger', 'danger', 'danger', 'danger'],
        id: 'error',
        specialFunction: function(current) {
            if (current.error === 0) {
                $('.snapb-' + current.id).hide();
                $('.camSSH-' + current.id).hide();
            } else {
                $('.snapb-' + current.id).show();
                $('.camSSH-' + current.id).show();
            }
        }
    }
}

var loadTile = function(tile, callback) {
    $.get('/tiles/' + tile + '.html', function(res, stat) {
        if (stat !== 'success')
            return;
        tiles[tile] = doT.template(res);
        callback();
    });
}

var render = function(tile, data, cb) {
    if (typeof tiles[tile] === 'function') {
        cb(tiles[tile].call(helpers, data));
    } else {
        loadTile(tile, function() {
            render(tile, data, cb);
        });
    }
}

var makeConfigErrorSummary = function(problems) {
    let out = '<table class="table table-striped">';
    for(let problem of problems) {
        out += `<tr><td style="font-weight: bold;">${problem[0]}</td><td>${problem[1]}</td></tr>`;
    }

    out += '</table>';
    return out;
}

var dataHandlers = function dataHandlers(data) {
    var pi = Pis[data.name];
    var transData = data.data;
    var handlers = {
        message: function() {
            console.log(transData);
            switch(transData.type) {
            case "config_input_error":
                swal('Configuration Errors', makeConfigErrorSummary(transData.details), 'error');
                break;
            default:
                swal(transData);
                break;
            }
        },
        snap: function() {
            var button = $('.snapb-' + pi.id);
            button.attr('onclick', 'snapper(this.name)');
            if (transData) {
                var tmp = '<a href="data:application/octet-stream;base64,' + transData + '" class="btn btn-default snap-' + pi.id + '" download="' + pi.name + ' ' + (new Date()).toLocaleTimeString().replace(/\//g, '-') + '.jpg"> ' + pi.name + ' ' + (new Date()).toLocaleTimeString().replace(/\//g, '-') + '.jpg</a>';
                $('.snap-' + pi.id).remove();
                $('.body-' + pi.id).append(tmp);
                button.html('Snapshot');
            } else {
                button.html('Retry');
            }
        },
        logs: function() {
            var logs = transData;
            render('logs', {
                pi: pi,
                logs: logs
            }, function(rendered) {
                $('.body-' + pi.id).append(rendered);
                $('.l-' + pi.id).show();
                $('.lc-' + pi.id).click(function() {
                    $('.l-' + pi.id).remove();
                });
            });
        },
        config: function() {
            pi.haveSettings = true;
            pi.config = transData;
            render('config', {
                pi: pi
            }, function(rendered) {
                $('.body-' + pi.id).append(rendered);
                $('.c-' + pi.id).show();
                $('.cc-' + pi.id).click(function() {
                    $('.c-' + pi.id).remove();
                });
                $('.cs-' + pi.id).click(function() {
                    var changes = {};
                    for (var prop of $('[id^="input-' + pi.id + '-"')) {
                        var property = prop.id.replace('input-' + pi.id + '-', '');
                        if($(prop).attr('type') === 'checkbox') {
                            prop.value = prop.checked;
                        }
                        if (`${transData[property]}` !== `${prop.value}`)
                            changes[property] = prop.value
                    }
                    if (Object.keys(changes).length > 0) {
                        command(pi.name, 'changeSettings', changes)
                        $('.c-' + pi.id).remove();
                    } else swal("Error", "No changes", "error")
                });
            });
        }
    }

    //call the handler
    var call = handlers[data.type];
    if (call)
        call();
}


var changeHandlers = function changeHandlers(what) {
    var pi = Pis[what.name];
    var change = what.change;
    var handlers = {
        piOn: function() {
            if (pi) {
                changeIt(what.data, Pis[what.name]);
                renderStatus(Pis[what.name]);
                piOnOff(Pis[what.name]);
            } else {
                Pis[what.name] = what.data;
                pi = Pis[what.name];
                addPi(pi);
            }
        },

        piOff: function() {
            renderStatus(pi);
            piOnOff(pi);
        },

        error: function() {
            renderStatus(pi);
        },

        startStop: function() {
            renderStatus(pi);
        },

        settings: function() {
            if (pi.haveSettings) {
                for (var prop in change) {
                    pi.config[prop] = change[prop];
                    var curr = $('#input-' + pi.id + '-' + prop);
                    if (curr.length === 1)
                        curr.val(change[prop])
                }
            }
        },

        deletePi: function() {
            $('.' + pi.id).remove();
            delete Pis[pi.name];
        }
    }

    function changeIt(change, level) {
        for (var key in change) {
            if (typeof change[key] === 'object') {
                if (!level[key])
                    level[key] = {};
                changeIt(change[key], level[key]);
            } else
                level[key] = change[key];
        }
    };

    if (change) {
        changeIt(change, pi);
    }

    //call the handler
    var call = handlers[what.type];
    if (call)
        call();
}

function piOnOff(current) {
    if (current.status === 1) {
        $('.body-' + current.id).hide();
    } else {
        $('.snapb-' + current.id).html('Snapshot');
        $('.body-' + current.id).show();
    }
}

function join(a, b) {
    for (var key in b) {
        a[key] = b[key];
    }
    return a;
}

function addPi(current, cb) {
    current.id = current.name.hashCode();
    render('pi', current, function(rendered) {
        $('.main').append(rendered);
        renderStatus(current);
        piOnOff(current);
        if (typeof cb == 'function') {
            cb();
        }
    });
}

function renderStatus(current) {
    render('statusBar', current, function(rendered) {
        $('.statBar-' + current.id).html(rendered);
    });
}

function getConfig(pi) {
    if (!Pis[pi].haveSettings)
        command(pi, 'config');
    else
        dataHandlers({
            type: 'config',
            data: Pis[pi].config,
            name: pi
        });
}

function restartSSH(pi) {
    command(pi, 'restartSSH');
}

function showSSH(pi) {
    let {
        ssh: {
            sshForwardPort
        },
        config: {
            sshPort,
            sshUser,
            sshMaster,
            sshLocalUser
        }
    } = Pis[pi];

    if(!sshForwardPort) {
        swal('No ssh connection!','','error');
        return;
    }

    swal({
        title: 'SSH Access',
        html: `<code>ssh -J ${sshUser}@${sshMaster}:${sshPort} ${sshLocalUser}@localhost -p ${sshForwardPort}<code>`
    });
}

function openCamPanel(pi) {
    if(! (pi in Pis))
        return;

    let {
        ssh: {
            camForwardPort,
        },
    } = Pis[pi];

    window.open(helpers.getPanelUrl(camForwardPort), '_blank');
}

function snapper(pi) {
    var current = Pis[pi];
    var button = $('.snapb-' + current.id);
    button.html('Loading...');
    button.attr('onclick', '');
    command(pi, 'snap');
}

function command(pi, command, data) {
    var tmp = {
        command: command
    }

    if (data)
        tmp.data = data;

    socket.emit('command', pi, tmp)
}

String.prototype.hashCode = function() {
    var hash = 0;
    if (this.length == 0) return hash;
    for (i = 0; i < this.length; i++) {
        char = this.charCodeAt(i);
        hash = ((hash << 5) - hash) + char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
}

function filterByHash() {
    var filterPi = decodeURI(window.location.hash.replace('#', ''));
    if (Pis[filterPi]) {
        $('.pi-r:not(.row-' + Pis[filterPi].id + ')').hide();
        document.title += ' - ' + Pis[filterPi].name;
    } else {
        $('.pi-r').show();
        document.title = 'Pi Control Center';
    }
}

//connect
var socket = io('/client');
socket.on('status', function(pis) {
    var pi, piNames;
    Pis = pis;
    $('.main').html("");
    piNames = Object.keys(Pis);
    pi = 0;

    function add() {
        var current = Pis[piNames[pi]];
        addPi(current, function() {
            pi += 1;
            if (pi < piNames.length) {
                add()
            } else {
                filterByHash();
            }
        });
    }

    if(piNames.length > 0)
        add();
});

$('.filter-btn').click(function() {
    var name = this.parentElement.children[0].children[0].value;
    $(".pi-r").show();
    $(".pi-r")
        .filter(function(index) {
            return this.attributes.name.textContent.toUpperCase().indexOf(name.toUpperCase()) === -1;
        }).hide()
});

$('.all-btn').click(function() {
    $(".pi-r").show();
});

$(window).on('hashchange', function() {
    filterByHash();
});

socket.on('change', function(what) {
    changeHandlers(what);
});

socket.on('data', function(data) {
    dataHandlers(data);
});
